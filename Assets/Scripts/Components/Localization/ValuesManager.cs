﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace Components.Localization {
    [ExecuteInEditMode]
    public class ValuesManager : MonoBehaviour {
        FileValueJSON FileValueJSON;
        FileValueJSON.ItensJSON _ItensJSON;
        FileSingleJSON.SingleJSON _SingleJSON;
        FileSingleJSON FileSingleJSON;

        private string _fileItem;
        private string _fileSingle = "ValuesSettings";

         [System.Serializable]
        private struct Item {
            public string key;
            public Text objText;
        }
        [SerializeField] private List<Item> _itens;
        [SerializeField] private string _path;

         [System.Serializable]
        public enum Values { Default }

        [SerializeField] Values _single;

         void Start () {
            FileValueJSON = new FileValueJSON ();
            FileSingleJSON = new FileSingleJSON ();

            _SingleJSON = FileSingleJSON.Load (ConcatStringFile (_fileSingle));

            _fileItem = _SingleJSON.values[(int) _single];

            _ItensJSON = FileValueJSON.Load (ConcatStringFile (_fileItem));

            SetContent ();
        }

        void SetContent () {
            foreach (var item in _itens) {
                foreach (var jsonItem in _ItensJSON.itens) {
                    if (jsonItem.key.Contains (item.key)) {
                        item.objText.fontSize = jsonItem.sizeFont;
                        item.objText.font = (Font)Resources.Load(jsonItem.nameFont, typeof(Font));
                    }
                }
            }
        }
        
        string ConcatStringFile (string nameFile) {
            string f = Application.dataPath + _path + nameFile + ".txt";
            if (!File.Exists (f)) {
                Debug.Log ("Nesse caminho " + f + " não contem arquivo Values.txt");
                return " ";
            }

            return f;
        }
    }
}