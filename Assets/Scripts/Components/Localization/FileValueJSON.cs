﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Components.Localization {
    public class FileValueJSON {
        /// <summary>
        /// Objecto JSON
        /// </summary>
        [System.Serializable]
        public class ElementJSON {
            /// <summary>
            /// Indentificador
            /// </summary>
            public string key;
            /// <summary>
            /// Tamanho da Fonte
            /// </summary>
            public int sizeFont;
            /// <summary>
            /// Nome da Fonte
            /// </summary>
            public string nameFont;
            /// <summary>
            /// Usado para expandir
            /// </summary>
            public bool show;

            /// <summary>
            /// Usadao para saber se vai usar cifrão
            /// </summary>
            public bool hasCipher;

            /// <summary>
            /// Tem casas decimais
            /// </summary>
            public bool hasDecimalPlace;

            /// <summary>
            /// Nome do cifrão
            /// </summary>
            public string cipher;
            /// <summary>
            /// Opções de tipo
            /// </summary>
            public enum DotOrComma {
                Virgula = 0,
                Ponto = 1
            }
            /// <summary>
            /// Usada para saber se é com virgula ou ponto
            /// </summary>
            public DotOrComma dotOrComma;
        }

        [System.Serializable]
        public class ItensJSON {
            public List<ElementJSON> itens;
        }
        /// <summary>
        /// Constrói um controle do arquivo 
        /// </summary>
        public FileValueJSON () { }

      
        /// <summary>
        /// Escreve as alterações no arquivo JSON
        /// </summary>
        /// <param name="itensJSON">Objecto JSON</param>
        public void SaveInJSON (ItensJSON itensJSON, string path) {;
            string json = JsonUtility.ToJson (itensJSON);
            File.WriteAllText (path, json);
        }

        /// <summary>
        /// Carrega o arquivo JSON
        /// </summary>
        /// <returns></returns>
        public ItensJSON Load (string path) {
            if (File.Exists (path)) {
                string fileJSON = File.ReadAllText (path);

                ItensJSON elementsJSONLoad = JsonUtility.FromJson<ItensJSON> (fileJSON);

                return elementsJSONLoad;
            }

            return null;
        }
        /// <summary>
        /// Cria um item e salva os dados em um objeto JSON
        /// </summary>
        /// <param name="_key">Indentificador</param>
        /// <param name="_sizeFont">Tamanho da Fonte</param>
        /// <param name="_nameFont"> Nome da Fonte</param>
        /// <param name="_file">Caminho + nome do arquivo</param>
        /// <param name="_cipher">Nome do cifrão</param>
        /// <param name="_dotOrComma">Virgula(0) ou Ponto(1)</param>
        /// <param name="_hasCipher">Se vai usar cifrão</param>
        /// <param name="_show">Usado para expandir no editor</param>
        public void Add (string _key, int _sizeFont, string _nameFont, string _file,
            string _cipher,
            int _dotOrComma,
            bool _hasDecimalPlace = false,
            bool _hasCipher = false,
            bool _show = false) {

            ElementJSON elementsJSON = new ElementJSON {
            key = _key,
            sizeFont = _sizeFont,
            nameFont = _nameFont,
            show = _show,
            hasCipher = _hasCipher,
            cipher = _cipher,
            dotOrComma = (ElementJSON.DotOrComma) _dotOrComma,
            hasDecimalPlace = _hasDecimalPlace

            };

            ItensJSON jLoad = Load (_file);
            ItensJSON itensJSON = new ItensJSON ();

            itensJSON.itens = new List<ElementJSON> ();

            foreach (ElementJSON elementLoad in jLoad.itens)
                itensJSON.itens.Add (elementLoad);

            itensJSON.itens.Add (elementsJSON);

            SaveInJSON (itensJSON, _file);
        }

        /// <summary>
        /// Deleta um item e salva os dados em um objeto JSON
        /// </summary>
        /// <param name="itensJSON">Objeto JSON</param>
        /// <param name="idx">Index do objeto</param>
        public void Delete (ItensJSON itensJSON, int idx, string _file) {
            itensJSON.itens.RemoveAt (idx);
            SaveInJSON (itensJSON, _file);
        }
      
    }
}