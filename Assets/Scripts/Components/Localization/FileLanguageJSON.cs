﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Components.Localization {
    /// <summary>
    /// Classe responsavel pela manipulação do arquivo JSON de idioma
    /// </summary>
    public class FileLanguageJSON {

        /// <summary>
        /// Objecto JSON
        /// </summary>
        [System.Serializable]
        public class ElementJSON {
            /// <summary>
            /// Indentificador
            /// </summary>
            public string key;
            /// <summary>
            /// Tamanho da Fonte
            /// </summary>
            public int sizeFont;
            /// <summary>
            /// Nome da Fonte
            /// </summary>
            public string nameFont;
            /// <summary>
            /// Conteudo a ser mostrado
            /// </summary>
            public string content;
            /// <summary>
            /// Usado para expandir
            /// </summary>
            public bool show;
        }

        [System.Serializable]
        public class ItensJSON {
            public List<ElementJSON> itens;
        }

        /// <summary>
        /// Constrói um controle do arquivo 
        /// </summary>
        public FileLanguageJSON () { }

        #region  Item
        /// <summary>
        /// Escreve as alterações no arquivo JSON
        /// </summary>
        /// <param name="itensJSON">Objecto JSON</param>
        public void SaveInJSON (ItensJSON itensJSON, string path) {;
            string json = JsonUtility.ToJson (itensJSON);
            File.WriteAllText (path, json);
        }

        /// <summary>
        /// Carrega o arquivo JSON
        /// </summary>
        /// <returns></returns>
        public ItensJSON Load (string path) {
            if (File.Exists (path)) {
                string fileJSON = File.ReadAllText (path);

                ItensJSON elementsJSONLoad = JsonUtility.FromJson<ItensJSON> (fileJSON);

                return elementsJSONLoad;
            }

            return null;
        }
        /// <summary>
        /// Cria um item e salva os dados em um objeto JSON
        /// </summary>
        /// <param name="_key">Indentificador</param>
        /// <param name="_sizeFont">Tamanho da Fonte</param>
        /// <param name="_nameFont"> Nome da Fonte</param>
        /// <param name="_content">Conteudo a ser mostrado</param>
        /// <param name="_show">Usado para expandir no editor</param>
        public void Add (string _key, int _sizeFont, string _nameFont, string _content, string _file, bool _show = false) {

            ElementJSON elementsJSON = new ElementJSON {
            key = _key,
            sizeFont = _sizeFont,
            nameFont = _nameFont,
            content = _content,
            show = _show
            };

            ItensJSON jLoad = Load (_file);
            ItensJSON itensJSON = new ItensJSON ();

            itensJSON.itens = new List<ElementJSON> ();

            foreach (ElementJSON elementLoad in jLoad.itens)
                itensJSON.itens.Add (elementLoad);

            itensJSON.itens.Add (elementsJSON);

            SaveInJSON (itensJSON, _file);
        }

        /// <summary>
        /// Deleta um item e salva os dados em um objeto JSON
        /// </summary>
        /// <param name="itensJSON">Objeto JSON</param>
        /// <param name="idx">Index do objeto</param>
        public void Delete (ItensJSON itensJSON, int idx, string _file) {
            itensJSON.itens.RemoveAt (idx);
            SaveInJSON (itensJSON, _file);
        }
        #endregion

    }
}