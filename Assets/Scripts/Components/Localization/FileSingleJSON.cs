﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Components.Localization {
    /// <summary>
    /// Classe responsavel pela manipulação do arquivo JSON sem atributos especificos
    /// </summary>
    public class FileSingleJSON {
        [System.Serializable]
        public class SingleJSON {
            public List<string> values;
        }

        /// <summary>
        /// Constrói um controle do arquivo 
        /// </summary>
        public FileSingleJSON () { }

        /// <summary>
        /// Cria um um valor e salva os dados em um objeto JSON
        /// </summary>
        /// <param name="_value">Valor</param>
        public void Add (string _value, string _file) {

            SingleJSON jLoad = Load (_file);
            SingleJSON JSON = new SingleJSON ();

            JSON.values = new List<string> ();

            foreach (string value in jLoad.values)
                JSON.values.Add (value);

            JSON.values.Add (_value);

            SaveInJSON (JSON, _file);
        }

        /// <summary>
        /// Carrega o arquivo JSON
        /// </summary>
        /// <returns></returns>
        public SingleJSON Load (string path) {

            if (File.Exists (path)) {
                string fileJSON = File.ReadAllText (path);

                SingleJSON JSONLoad = JsonUtility.FromJson<SingleJSON> (fileJSON);

                return JSONLoad;
            }

            return null;
        }

        /// <summary>
        /// Escreve as alterações no arquivo JSON
        /// </summary>
        /// <param name="JSON">Objeto JSON</param>
        public void SaveInJSON (SingleJSON JSON, string path) {
            string json = JsonUtility.ToJson (JSON);
            File.WriteAllText (path, json);
        }
        
    }
}