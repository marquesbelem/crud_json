﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace Components.Localization {
    [ExecuteInEditMode]
    public class LanguageManager : MonoBehaviour {

        FileLanguageJSON FileLanguageJSON;
        FileLanguageJSON.ItensJSON _ItensJSON;
        FileSingleJSON.SingleJSON _SingleJSON;
        FileSingleJSON FileSingleJSON;

        private string _fileItem;
        private string _fileLanguage = "Language";

        [System.Serializable]
        private struct Item {
            public string key;
            public Text objText;
        }

        [SerializeField] private List<Item> _itens;

        [System.Serializable]
        public enum Languages { English, Spanish, Portuguese }

        [SerializeField] Languages _languages;

        [SerializeField] private string _path;

        void Start () {
            FileLanguageJSON = new FileLanguageJSON ();
            FileSingleJSON = new FileSingleJSON ();

            _SingleJSON = FileSingleJSON.Load (ConcatStringFile (_fileLanguage));

            _fileItem = _SingleJSON.values[(int) _languages];

            _ItensJSON = FileLanguageJSON.Load (ConcatStringFile (_fileItem));

            SetContent ();
        }

        void SetContent () {
            foreach (var item in _itens) {
                foreach (var jsonItem in _ItensJSON.itens) {
                    if (jsonItem.key.Contains (item.key)) {
                        item.objText.text = jsonItem.content;
                        item.objText.fontSize = jsonItem.sizeFont;
                        item.objText.font = (Font)Resources.Load(jsonItem.nameFont, typeof(Font));
                    }
                }
            }
        }

        string ConcatStringFile (string nameFile) {
            string f = Application.dataPath + _path + nameFile + ".txt";
            if (!File.Exists (f)) {
                Debug.Log ("Nesse caminho " + f + " não contem arquivo Language");
                return " ";
            }

            return f;
        }
    }
}