﻿using System.Collections;
using System.Collections.Generic;
using Components.Localization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Editor;
using static UnityEditor.EditorGUILayout;
using System;
using System.IO;

public class ValueEditor : EditorWindow {
    private string _fileSingle = "ValuesSettings";
    private int _cipherDefault;
    private int _dotOrCommaDefault;
    private string _pathSave;
    private string _fileItem;
    private string _keySingle;
    private string _key;
    private Vector2 _scrollPos;
    private bool _refresh = true;
    private int _selected;
    private int _idxHasCipher;
    private int _idxHasDecimalPlace;
    FileValueJSON FileValueJSON;
    FileValueJSON.ItensJSON _ItensJSON;
    FileSingleJSON.SingleJSON _SingleJSON;
    FileSingleJSON.SingleJSON _CipherSingleJSON;
    FileSingleJSON FileSingleJSON;
    public string[] choices = new string[] { "False", "True" };
    public string[] choicesDotOrComma = new string[] { ",", "." };

    [MenuItem ("Camis/Value Settings")]
    public static void Open () {
        GetWindow<ValueEditor> ("Value Window");
    }

    void OnEnable () {
        if (string.IsNullOrEmpty (_pathSave))
            return;

        LoadJson ();

        for (int i = 0; i < _ItensJSON.itens.Count; i++) {
            _ItensJSON.itens[i].show = false;
            FileValueJSON.SaveInJSON (_ItensJSON, _fileItem);
        }

    }
    void OnGUI () {

        FolderSingle ();

        if (string.IsNullOrEmpty (_pathSave) || !File.Exists (ConcatStringFile (_fileSingle)))
            return;

        Add ();

        Selected ();

        Space ();
        Space ();

        AddItem ();

        _scrollPos = BeginScrollView (_scrollPos, GUILayout.Width (100), GUILayout.Height (750));

        for (int i = 0; i < _ItensJSON.itens.Count; i++) {
            BeginHorizontal ();

            Separator ();
            _ItensJSON.itens[i].show = Foldout (_ItensJSON.itens[i].show, _ItensJSON.itens[i].key);

            if (_ItensJSON.itens[i].show) {
                FileValueJSON.SaveInJSON (_ItensJSON, _fileItem);

                BeginVertical (GUILayout.MaxWidth (800));
                Space ();
                GUILayout.Label (" ");

                EditorGUI.BeginChangeCheck ();
                if (GUILayout.Button (string.IsNullOrEmpty (_ItensJSON.itens[i].nameFont) ? "Select the font" : _ItensJSON.itens[i].nameFont)) {
                    if (EditorGUI.EndChangeCheck ()) {
                        _ItensJSON.itens[i].nameFont = EditorUtility.OpenFilePanel ("Select the font", "", "ttf");
                        if (string.IsNullOrEmpty (_ItensJSON.itens[i].nameFont)) {
                            Debug.Log ("Fonte não selecionada");
                        } else {
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace (Application.dataPath, "");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace ("Resources/", "");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace (".ttf", "");

                            int found = _ItensJSON.itens[i].nameFont.IndexOf ("/");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Remove (found, 1);
                        }
                    }
                }

                _ItensJSON.itens[i].sizeFont = IntField ("Size Font: ", _ItensJSON.itens[i].sizeFont);

                _idxHasCipher = Popup ("Has Cipher", _idxHasCipher, choices);
                _ItensJSON.itens[i].hasCipher = Convert.ToBoolean (_idxHasCipher);

                _ItensJSON.itens[i].cipher = _CipherSingleJSON.values[_cipherDefault];
                _ItensJSON.itens[i].dotOrComma = (FileValueJSON.ElementJSON.DotOrComma) _dotOrCommaDefault;

                _idxHasDecimalPlace = Popup ("Has decimal place", _idxHasDecimalPlace, choices);
                _ItensJSON.itens[i].hasDecimalPlace = Convert.ToBoolean (_idxHasDecimalPlace);

                Space ();

                BeginHorizontal ();
                if (GUILayout.Button ("Delete", GUILayout.Width (100))) {
                    FileValueJSON.Delete (_ItensJSON, i, _fileItem);
                    _ItensJSON = FileValueJSON.Load (_fileItem);
                }

                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");

                if (GUILayout.Button ("Apply", GUILayout.Width (100)))
                    FileValueJSON.SaveInJSON (_ItensJSON, _fileItem);

                Space ();
                GUILayout.Label (" ");

                EndHorizontal ();
                EndVertical ();
            }

            Space ();
            EndHorizontal ();
        }

        EndScrollView ();

        BeginHorizontal ();
    }

    void AddItem () {
        BeginHorizontal (GUILayout.MaxWidth (500));

        _key = TextField ("Key name: ", _key);

        if (GUILayout.Button ("Add Item")) {
            FileValueJSON.Add (_key, 0, "", _fileItem, "$", 0, true, true);
            _key = " ";
            _ItensJSON = FileValueJSON.Load (_fileItem);
        }

        EndHorizontal ();
    }

    void Add () {
        BeginHorizontal (GUILayout.MaxWidth (400));

        _keySingle = TextField ("New Value: ", _keySingle);
        if (GUILayout.Button ("Add Value")) {
            FileSingleJSON.Add (_keySingle, ConcatStringFile (_fileSingle));
            _keySingle = " ";

            _SingleJSON = FileSingleJSON.Load (ConcatStringFile (_fileSingle));
            _selected = _SingleJSON.values.Count - 1;
            _fileItem = ConcatStringFile (_SingleJSON.values[_selected]);

            FileValueJSON.ItensJSON ItensJSONTmp = _ItensJSON;
            FileValueJSON.SaveInJSON (ItensJSONTmp, _fileItem);

            _ItensJSON = FileValueJSON.Load (_fileItem);
        }

        EndHorizontal ();
    }

    void Selected () {

        BeginHorizontal (GUILayout.MaxWidth (500));
        EditorGUI.BeginChangeCheck ();

        _selected = Popup ("Selected File Profile", _selected, _SingleJSON.values.ToArray ());

        if (EditorGUI.EndChangeCheck ()) {
            _fileItem = ConcatStringFile (_SingleJSON.values[_selected]);
            _ItensJSON = FileValueJSON.Load (_fileItem);

            for (int i = 0; i < _ItensJSON.itens.Count; i++) {
                _ItensJSON.itens[i].show = false;
                FileValueJSON.SaveInJSON (_ItensJSON, _fileItem);
            }
        }

        EndHorizontal ();

        BeginHorizontal (GUILayout.MaxWidth (500));
        BeginVertical ();

        _cipherDefault = Popup ("Selected Cipher: ", _cipherDefault, _CipherSingleJSON.values.ToArray ());
        _dotOrCommaDefault = Popup ("Selected Comma or Dot: ", _dotOrCommaDefault, choicesDotOrComma);
        EndVertical ();
        EndHorizontal ();
    }

    void FolderSingle () {
        EditorGUI.BeginChangeCheck ();
        if (GUILayout.Button (string.IsNullOrEmpty (_pathSave) ? "Select the folder" : _pathSave)) {
            if (EditorGUI.EndChangeCheck ()) {
                _pathSave = EditorUtility.OpenFolderPanel ("Folder", Application.dataPath, "");
                if (string.IsNullOrEmpty (_pathSave)) {
                    Debug.Log ("Pasta não selecionada");
                } else {
                    _pathSave = _pathSave.Replace (Application.dataPath, "") + "/";
                    LoadJson ();
                }
            }
        }
    }

    void LoadJson () {
        FileValueJSON = new FileValueJSON ();
        FileSingleJSON = new FileSingleJSON ();

        string f = ConcatStringFile (_fileSingle);

        if (!File.Exists (f)) {
            Debug.Log ("Nenhum arquivo Values.txt encontrado");
            return;
        }

        _SingleJSON = FileSingleJSON.Load (f);
        _CipherSingleJSON = FileSingleJSON.Load (ConcatStringFile("Cipher"));
        _fileItem = ConcatStringFile (_SingleJSON.values[0]);
        _ItensJSON = FileValueJSON.Load (_fileItem);
    }

    string ConcatStringFile (string nameFile) {
        return Application.dataPath + _pathSave + nameFile + ".txt";
    }
}