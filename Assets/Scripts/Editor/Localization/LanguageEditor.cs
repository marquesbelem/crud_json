﻿using System.Collections;
using System.Collections.Generic;
using Components.Localization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Editor;
using static UnityEditor.EditorGUILayout;
using System.IO;

public class LanguageEditor : EditorWindow {

    private string _pathSave;
    private string _fileItem;
    private string _fileLanguage = "Language";
    private string _keyLanguage;
    private string _key;
    private Font _font;
    private Vector2 _scrollPos;
    private bool _refresh = true;
    private int _selected;
    FileLanguageJSON FileLanguageJSON;
    FileLanguageJSON.ItensJSON _ItensJSON;
    FileSingleJSON.SingleJSON _SingleJSON;
    FileSingleJSON FileSingleJSON;

    [MenuItem ("Camis/Language Settings")]
    public static void Open () {
        GetWindow<LanguageEditor> ("Language Window");
    }

    void OnEnable () {
        if (string.IsNullOrEmpty (_pathSave))
            return;

        LoadJsonLanguage ();

        for (int i = 0; i < _ItensJSON.itens.Count; i++) {
            _ItensJSON.itens[i].show = false;
            FileLanguageJSON.SaveInJSON (_ItensJSON, _fileItem);
        }

    }
    void OnGUI () {

        FolderLanguage ();

        if (string.IsNullOrEmpty (_pathSave) || !File.Exists (ConcatStringFile (_fileLanguage)))
            return;

        AddLanguage ();

        SelectedLanguage ();

        Space ();
        Space ();

        AddItem ();

        _scrollPos = BeginScrollView (_scrollPos, GUILayout.Width (100), GUILayout.Height (750));

        for (int i = 0; i < _ItensJSON.itens.Count; i++) {
            BeginHorizontal ();

            Separator ();
            _ItensJSON.itens[i].show = Foldout (_ItensJSON.itens[i].show, _ItensJSON.itens[i].key);

            if (_ItensJSON.itens[i].show) {
                FileLanguageJSON.SaveInJSON (_ItensJSON, _fileItem);

                BeginVertical (GUILayout.MaxWidth (800));
                Space ();
                GUILayout.Label (" ");

                EditorGUI.BeginChangeCheck ();
                if (GUILayout.Button (string.IsNullOrEmpty (_ItensJSON.itens[i].nameFont) ? "Select the font" : _ItensJSON.itens[i].nameFont)) {
                    if (EditorGUI.EndChangeCheck ()) {
                        _ItensJSON.itens[i].nameFont = EditorUtility.OpenFilePanel ("Select the font", "", "ttf");
                        if (string.IsNullOrEmpty (_ItensJSON.itens[i].nameFont)) {
                            Debug.Log ("Fonte não selecionada");
                        } else {
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace (Application.dataPath, "");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace ("Resources/", "");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Replace (".ttf", "");                           

                            int found = _ItensJSON.itens[i].nameFont.IndexOf ("/");
                            _ItensJSON.itens[i].nameFont = _ItensJSON.itens[i].nameFont.Remove (found, 1);
                        }
                    }
                }

                _ItensJSON.itens[i].sizeFont = IntField ("Size Font: ", _ItensJSON.itens[i].sizeFont);
                _ItensJSON.itens[i].content = TextField ("Content: ", _ItensJSON.itens[i].content);

                Space ();

                BeginHorizontal ();
                if (GUILayout.Button ("Delete", GUILayout.Width (100))) {
                    FileLanguageJSON.Delete (_ItensJSON, i, _fileItem);
                    _ItensJSON = FileLanguageJSON.Load (_fileItem);
                }

                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");
                Space ();
                GUILayout.Label (" ");

                if (GUILayout.Button ("Apply", GUILayout.Width (100)))
                    FileLanguageJSON.SaveInJSON (_ItensJSON, _fileItem);

                Space ();
                GUILayout.Label (" ");

                EndHorizontal ();
                EndVertical ();
            }

            Space ();
            EndHorizontal ();
        }

        EndScrollView ();

        BeginHorizontal ();
    }

    void AddItem () {
        BeginHorizontal (GUILayout.MaxWidth (500));

        _key = TextField ("Key name: ", _key);

        if (GUILayout.Button ("Add Item")) {
            FileLanguageJSON.Add (_key, 0, "", "new content", _fileItem, true);
            _key = " ";
            _ItensJSON = FileLanguageJSON.Load (_fileItem);
        }

        EndHorizontal ();
    }

    void AddLanguage () {
        BeginHorizontal (GUILayout.MaxWidth (400));

        _keyLanguage = TextField ("New Language: ", _keyLanguage);
        if (GUILayout.Button ("Add Language")) {
            FileSingleJSON.Add (_keyLanguage, ConcatStringFile (_fileLanguage));
            _keyLanguage = " ";

            _SingleJSON = FileSingleJSON.Load (ConcatStringFile (_fileLanguage));
            _selected = _SingleJSON.values.Count - 1;
            _fileItem = ConcatStringFile (_SingleJSON.values[_selected]);

            FileLanguageJSON.ItensJSON ItensJSONTmp = _ItensJSON;
            FileLanguageJSON.SaveInJSON (ItensJSONTmp, _fileItem);

            _ItensJSON = FileLanguageJSON.Load (_fileItem);
        }

        EndHorizontal ();
    }

    void SelectedLanguage () {

        BeginHorizontal (GUILayout.MaxWidth (500));
        EditorGUI.BeginChangeCheck ();

        _selected = Popup ("Selected Language", _selected, _SingleJSON.values.ToArray ());

        if (EditorGUI.EndChangeCheck ()) {
            _fileItem = ConcatStringFile (_SingleJSON.values[_selected]);
            _ItensJSON = FileLanguageJSON.Load (_fileItem);

            for (int i = 0; i < _ItensJSON.itens.Count; i++) {
                _ItensJSON.itens[i].show = false;
                FileLanguageJSON.SaveInJSON (_ItensJSON, _fileItem);
            }
        }

        EndHorizontal ();
    }

    void FolderLanguage () {
        EditorGUI.BeginChangeCheck ();
        if (GUILayout.Button (string.IsNullOrEmpty (_pathSave) ? "Select the folder" : _pathSave)) {
            if (EditorGUI.EndChangeCheck ()) {
                _pathSave = EditorUtility.OpenFolderPanel ("Folder", Application.dataPath, "");
                if (string.IsNullOrEmpty (_pathSave)) {
                    Debug.Log ("Pasta não selecionada");
                } else {
                    _pathSave = _pathSave.Replace (Application.dataPath, "") + "/";
                    LoadJsonLanguage ();
                }
            }
        }
    }

    void LoadJsonLanguage () {
        FileLanguageJSON = new FileLanguageJSON ();
        FileSingleJSON = new FileSingleJSON();
        
        string f = ConcatStringFile (_fileLanguage);

        if (!File.Exists (f)) {
            Debug.Log ("Nenhum arquivo Language.txt encontrado");
            return;
        }

        _SingleJSON = FileSingleJSON.Load (f);
        _fileItem = ConcatStringFile (_SingleJSON.values[0]);
        _ItensJSON = FileLanguageJSON.Load (_fileItem);
    }

    string ConcatStringFile (string nameFile) {
        return Application.dataPath + _pathSave + nameFile + ".txt";
    }
}